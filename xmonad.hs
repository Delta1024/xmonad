import Data.Ratio
import XMonad
import XMonad.Actions.FindEmptyWorkspace
import XMonad.Actions.GridSelect
import XMonad.Actions.NoBorders
import XMonad.Actions.UpdateFocus
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.FloatNext
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import Control.Monad (when)
import System.Exit
import System.Environment (getArgs)
import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce
import XMonad.Util.WorkspaceCompare
import XMonad.Util.Replace (replace)
import XMonad.Util.Run
import qualified XMonad.StackSet as W
import qualified DBus as D
import qualified DBus.Client as D

myLayouts = avoidStruts  $ smartBorders $
 layoutDefault ||| Mirror layoutDefault ||| layoutSpacing Full ||| noBorders Full
 where
 layoutTall    = Tall 1 (3/100) (1/2)
 layoutSpacing = spacingRaw False (Border 5 5 5 5) True (Border 5 5 5 5) True
 layoutDefault = layoutSpacing $ layoutTall

windowCount :: X (Maybe String)
windowCount   = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset
myTerminal    = "alacritty"
myModMask     = mod4Mask
myBorderWidth = 2
myBorderColor = "#228B22"
myEventHook   = handleEventHook def <+> XMonad.Hooks.EwmhDesktops.fullscreenEventHook
myWorkspaces  = ["main","2","3","4","5","6","7","8","9"]
myFileManager :: String
myFileManager = "pcmanfm"
terminalFM    :: String
terminalFM    = "alacritty -e ranger"
configScript :: String
configScript = "/home/jake/.dmenu/Configs/configs.sh"
picomToggle :: String
picomToggle = "/home/jake/.xmonad/scripts/picom-toggle.sh"
openAgenda :: String
openAgenda = "emacsclient -c --eval '(org-agenda nil \"n\")'"

myManageHook  = floatNextHook <+> composeAll
  [ className =? "Barrier"                             --> doFloat
  ,(className =? "Brave" <&&> resource =? "brave"    ) --> doFloat --xprop WM_CLASS resource, Class
  -- ,(className =? "Lutris" <&&> resource=? "lutris"   ) --> doFloat
  , className =? "pavucontrol-qt"                      --> doFloat
  ,(className =? "Steam" <&&> title =? "Friends List") --> doFloat  --xprop WM_NAME for title name.
  ,(className =? "Steam" <&&> title =? "Steam - News*") --> doFloat
  ]

myKeybindings = [

      ( "M-;"   , spawn "betterlockscreen -l")
    , ( "M-o"   , spawn "emacsclient -c"     )
    , ( "M-S-o" , spawn openAgenda           )
    , ( "M-i b" , spawn "brave"              )
    , ( "M-i q" , spawn "qutebrowser"        )
    , ( "M-s s" , spawn "steam"              )
    , ( "M-s l" , spawn "lutris"             )
    , ( "M-/"   , spawn "thunderbird"        )

    , ( "M-M1-<Return>" , spawn "dmenu_run -c -l 10"          )
    , ( "M-S-<Return>"  , spawn myTerminal                    )
    , ( "M-<Delete>"    , spawn myFileManager                 )
    , ( "M-x"           , spawn terminalFM                    )
    , ( "M-S-b"         , spawn "killall xmobar trayer"       )
    , ( "C-M1-<Delete>" , spawn "alacritty -e bashtop"        )
    , ( "M-g"           , withFocused toggleBorder            )
    , ( "M-C-g"         , toggleWindowSpacingEnabled          )
    , ( "M-S-g"         , toggleScreenSpacingEnabled          )
    , ( "M-f"           , toggleFloatNext                     )
    , ( "M-S-j"         , goToSelected def                    )
    , ( "M-S-k"         , bringSelected def                   )
    , ( "M-C-m"         , viewEmptyWorkspace                  )
    , ( "M-S-m"         , tagToEmptyWorkspace                 )
    , ( "M-p"           , spawn  picomToggle                  )
    , ( "M-M1-`"        , spawn configScript                  )

    , ( "M-<F10>" , spawn "/home/jake/.xmonad/scripts/screens/left.sh" )
    , ( "M-<F11>" , spawn "/home/jake/.xmonad/scripts/screens/both.sh" )
    , ( "M-<F12>" , spawn "/home/jake/.xmonad/scripts/screens/right.sh")
    --, ( "M-<F9>" , spawn "/home/jake/.scripts/vm.sh")

    , ( "M-S-q q" , io (exitWith ExitSuccess))
    , ( "M-S-q r" , spawn "reboot")
    , ( "M-S-q p" , spawn "poweroff")
    ]

myStartupHook = do
    spawnOnce "nitrogen --restore"
    spawnOnce "emacs --daemon"
    spawnOnce "picom --config $HOME/.xmonad/scripts/picom.conf"
    --spawnOnce "redshift&"
    spawnOnce "nm-applet &"
    spawnOnce "volumeicon &"
    spawnOnce "blueberry-tray&"
    spawnOnce "numlockx on &"
    spawnOnce "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &"
    spawnOnce "/usr/lib/xfce4/notifyd/xfce4-notifyd &"
    spawnOnce "pamac-tray &"
    spawn     "/home/jake/.xmonad/scripts/trayer-check.sh"
--    spawnOnce "xfce4-clipman"
    adjustEventInput

main = do
  dbus <- D.connectSession
   -- request acces to the DBus name
  D.requestName dbus (D.busName_ "org.xmonad.Log")
       [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]
  xmproc0 <- spawnPipe "xmobar -x 0 /home/jake/.xmonad/xmobar/xmobar0rc"
  xmproc1 <- spawnPipe "xmobar -x 1 /home/jake/.xmonad/xmobar/xmobar1rc"
  args <- getArgs
  when ( "--replace" `elem` args ) replace
  xmonad $ ewmh $ docks desktopConfig
    { handleEventHook    = myEventHook
    , terminal           = myTerminal
    , modMask            = myModMask
    , logHook            = dynamicLogWithPP $ def { ppOutput = \x -> hPutStrLn xmproc0 x  >> hPutStrLn xmproc1 x
                                                  , ppCurrent = xmobarColor "#007111" "" . wrap "[" "]"
                                                  , ppVisible = xmobarColor "#228B22" "" -- . wrap "{" "}"
                                                  , ppSort = getSortByXineramaRule
                                                  , ppHidden = xmobarColor "#EA3612" "" . wrap "*" ""   -- Hidden workspaces in xmobar
                                                  , ppHiddenNoWindows = xmobarColor "#008b8b" ""
                                                  , ppUrgent = xmobarColor "#E67E22" "" . wrap "!" "!"
                                                  , ppExtras  = [windowCount]
                                                  , ppSep =  " | "
                                                  , ppTitle = xmobarColor "#b3afc2" "" . shorten 60

                                                  }
    , borderWidth        = myBorderWidth
    , focusedBorderColor = myBorderColor
    , layoutHook         = myLayouts
    , manageHook         = myManageHook
    , startupHook        = myStartupHook
    , workspaces         = myWorkspaces
    } `additionalKeysP`  myKeybindings
