#+TITLE: Xmobar Config
   ____  _____ _   _____  _    _  ___ ____  _  _
  |  _ \| ____| | |_   _|/ \  / |/ _ \___ \| || |
  | | | |  _| | |   | | / _ \ | | | | |__) | || |_
  | |_| | |___| |___| |/ ___ \| | |_| / __/|__   _|
  |____/|_____|_____|_/_/   \_\_|\___/_____|  |_|

* TABLE OF CONTENCE :toc:
- [[#main-screen][Main Screen]]
  - [[#appearance][appearance]]
  - [[#layout][layout]]
  - [[#plugins][plugins]]
- [[#second-screen][Second Screen]]
  - [[#appearance-1][appearance]]
  - [[#layout-1][layout]]
  - [[#general-behavior][general behavior]]
  - [[#plugins-1][plugins]]

* Main Screen
#+BEGIN_SRC haskell #+header-args: :tangle xmobar0rc
Config {
#+END_SRC
** appearance
#+BEGIN_SRC haskell #+header-args: :tangle xmobar0rc
     font =         "xft:Bitstream Vera Sans Mono:size=9:bold:antialias=true"
   , bgColor =      "black"
   , alpha   =      220
   , fgColor =      "#646464"
   , position =     TopP 0 0
   , border =       BottomB
   , borderColor =  "black"
#+END_SRC
** layout
#+BEGIN_SRC haskell #+header-args: :tangle xmobar0rc
   , sepChar =  "%"   -- delineator between plugin names and straight text
   , alignSep = "}{"  -- separator between left-right alignment
   , template = "%date% %StdinReader% }{ %CYEG%  <fc=#4682B4>-</fc> <fc=#8d327f> %multicpu% - %memory% - %dynnetwork%  %trayerpad% %trayercheck%</fc>" -- General behavior
   , lowerOnStart =     True    -- send to bottom of window stack on start
   , hideOnStart =      False   -- start with window unmapped (hidden)
   , allDesktops =      False   -- show on all desktops
   , overrideRedirect = True    -- set the Override Redirect flag (Xlib)
   , pickBroadest =     False   -- choose widest display (multi-monitor)
   , persistent =       True    -- enable/disable hiding (True = disabled)
#+END_SRC
** plugins
   Numbers can be automatically colored according to their value. xmobar
   decides color based on a three-tier/two-cutoff system, controlled by
   command options:
       --Low sets the low cutoff
       --High sets the high cutoff

       --low sets the color below --Low cutoff
       --normal sets the color between --Low and --High cutoffs
       --High sets the color above --High cutoff

     The --template option controls how the plugin is displayed. Text
     color can be set by enclosing in <fc></fc> tags. For more details
     see http://projects.haskell.org/xmobar/#system-monitor-plugins.
#+BEGIN_SRC haskell #+header-args: :tangle xmobar0rc
   , commands =
#+END_SRC

*** weather monitor
#+BEGIN_SRC haskell #+header-args: :tangle xmobar0rc
        [ Run Weather "CYEG" [ "--template", "<skyCondition> | <fc=#4682B4><tempC></fc>°C | <fc=#4682B4><rh></fc>% | <fc=#4682B4><pressure></fc>hPa"
                            ] 36000
#+END_SRC
*** network activity monitor (dynamic interface resolution)
#+BEGIN_SRC haskell #+header-args: :tangle xmobar0rc
        , Run DynNetwork     [ "--template" , "<dev>: <tx>kB/s|<rx>kB/s"
                             , "--Low"      , "1000"       -- units: B/s
                             , "--High"     , "5000"       -- units: B/s
                             , "--low"      , "darkorange"
                             , "--normal"   , "darkgreen"
                             , "--high"     , "darkred"
                             ] 10
#+END_SRC
*** cpu activity monitor
#+BEGIN_SRC haskell #+header-args: :tangle xmobar0rc
        , Run MultiCpu       [ "--template" , "Cpu: <total0>%|<total1>%"
                             , "--Low"      , "50"         -- units: %
                             , "--High"     , "85"         -- units: %
                             , "--low"      , "darkorange"
                             , "--normal"   , "darkgreen"
                             , "--high"     , "darkred"
                             ] 10
#+END_SRC
*** cpu core temperature monitor
#+BEGIN_SRC haskell #+header-args: :tangle xmobar0rc
        , Run CoreTemp       [ "--template" , "Temp: <core0>°C|<core1>°C"
                             , "--Low"      , "70"        -- units: °C
                             , "--High"     , "80"        -- units: °C
                             , "--low"      , "darkorange"
                             , "--normal"   , "darkgreen"
                             , "--high"     , "darkred"
                             ] 50
#+END_SRC
*** memory usage monitor
#+BEGIN_SRC haskell #+header-args: :tangle xmobar0rc
        , Run Memory         [ "--template" ,"Mem: <used>mB/s"
                             , "--Low"      , "4000"        -- units: %
                             , "--High"     , "14000"        -- units: %
                             , "--low"      , "darkorange"
                             , "--normal"   , "darkgreen"
                             , "--high"     , "darkred"
                             ] 10
#+END_SRC
*** time/date indicator and trayer
(%F = y-m-d date, %a = day of week, %T = h:m:s time)
#+BEGIN_SRC haskell #+header-args: :tangle xmobar0rc
        , Run Date           "<fc=#ABABAB>%F (%a) %r</fc>" "date" 10
        , Run StdinReader
        , Run Com "/home/jake/.xmonad/scripts/trayer-padding-icon.sh" [] "trayerpad" 10
        , Run Com "/home/jake/.xmonad/scripts/trayer-check.sh"        [] "trayercheck" 10
        ]
   }
#+END_SRC

* Second Screen
#+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
Config {
#+END_SRC
** appearance
#+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
     font =         "xft:Bitstream Vera Sans Mono:size=9:bold:antialias=true"
   , bgColor =      "black"
   , alpha   =      220
   , fgColor =      "#646464"
   , position =     Top
   , border =       BottomB
   , borderColor =  "black"
#+END_SRC
** layout
#+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
   , sepChar =  "%"   -- delineator between plugin names and straight text
   , alignSep = "}{"  -- separator between left-right alignment
   , template = "%date% %StdinReader% }{<fc=#8d327f> %multicpu% - %memory% - %dynnetwork%</fc> "
--   , template = "%date% %StdinReader% }{%CYEG%  <fc=#4682B4>-</fc> <fc=#8d327f> %multicpu% - %memory% - %dynnetwork%</fc> "
#+END_SRC
** general behavior
#+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
   , lowerOnStart =     True    -- send to bottom of window stack on start
   , hideOnStart =      False    -- start with window unmapped (hidden)
   , allDesktops =      False   -- show on all desktops
   , overrideRedirect = True    -- set the Override Redirect flag (Xlib)
   , pickBroadest =     True   -- choose widest display (multi-monitor)
   , persistent =       True  -- enable/disable hiding (True = disabled)
#+END_SRC
** plugins
  Numbers can be automatically colored according to their value. xmobar
  decides color based on a three-tier/two-cutoff system, controlled by
  command options:
  Low sets the low cutoff
  --High sets the high cutoff
   --low sets the color below --Low cutoff
   --normal sets the color between --Low and --High cutoffs
   --High sets the color above --High cutoff

  The --template option controls how the plugin is displayed. Text
  color can be set by enclosing in <fc></fc> tags. For more details
  see http://projects.haskell.org/xmobar/#system-monitor-plugins.
  #+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
   , commands =
  #+END_SRC
*** weather monitor
#+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
 --       [ Run Weather "CYEG" [ "--template", "<skyCondition> | <fc=#4682B4><tempC></fc>°C | <fc=#4682B4><rh></fc>% | <fc=#4682B4><pressure></fc>hPa"
  --                           ] 36000
#+END_SRC
*** network activity monitor (dynamic interface resolution)
#+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
        [ Run DynNetwork     [ "--template" , "<dev>: <tx>kB/s|<rx>kB/s"
                             , "--Low"      , "1000"       -- units: B/s
                             , "--High"     , "5000"       -- units: B/s
                             , "--low"      , "darkgreen"
                             , "--normal"   , "darkorange"
                             , "--high"     , "darkred"
                             ] 10
#+END_SRC
*** cpu activity monitor
#+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
        , Run MultiCpu       [ "--template" , "Cpu: <total0>%|<total1>%"
                             , "--Low"      , "50"         -- units: %
                             , "--High"     , "85"         -- units: %
                             , "--low"      , "darkgreen"
                             , "--normal"   , "darkorange"
                             , "--high"     , "darkred"
                             ] 10
#+END_SRC
*** cpu core temperature monitor
#+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
        , Run CoreTemp       [ "--template" , "Temp: <core0>°C|<core1>°C"
                             , "--Low"      , "70"        -- units: °C
                             , "--High"     , "80"        -- units: °C
                             , "--low"      , "darkgreen"
                             , "--normal"   , "darkorange"
                             , "--high"     , "darkred"
                             ] 50
#+END_SRC
*** memory usage monitor
#+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
        , Run Memory         [ "--template" ,"Mem: <used>mB/s"
                             , "--Low"      , "4000"        -- units: %
                             , "--High"     , "14000"        -- units: %
                             , "--low"      , "darkgreen"
                             , "--normal"   , "darkorange"
                             , "--high"     , "darkred"
                             ] 10
#+END_SRC
*** time/date indicator and trayer
(%F = y-m-d date, %a = day of week, %T = h:m:s time)
#+BEGIN_SRC haskell #+header-args: :tangle xmobar1rc
        , Run Date           "<fc=#ABABAB>%F (%a) %r</fc>" "date" 10
        , Run StdinReader
        ]
   }
#+END_SRC
