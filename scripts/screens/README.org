#+TITLE: Screen Control SCripts
   ____  _____ _   _____  _    _  ___ ____  _  _
  |  _ \| ____| | |_   _|/ \  / |/ _ \___ \| || |
  | | | |  _| | |   | | / _ \ | | | | |__) | || |_
  | |_| | |___| |___| |/ ___ \| | |_| / __/|__   _|
  |____/|_____|_____|_/_/   \_\_|\___/_____|  |_|

* TABLE OF CONTENSE :toc:
- [[#left][Left]]
- [[#both][Both]]
- [[#right][Right]]

* Left
#+BEGIN_SRC shell #+header-args: :tangle left.sh
#!/bin/bash
leftDisplay=DisplayPort-1
rightDisplay=HDMI-A-0
xrandr --output ${leftDisplay} --auto --output ${rightDisplay} --off
nitrogen --restore
#+END_SRC
* Both
#+BEGIN_SRC shell #+header-args: :tangle both.sh
#!/bin/bash
leftDisplay=DisplayPort-1
rightDisplay=HDMI-A-0
xrandr --output ${leftDisplay} --auto --output ${rightDisplay} --auto --right-of ${leftDisplay}
nitrogen --restore
#+END_SRC
* Right
#+BEGIN_SRC shell #+header-args: :tangle right.sh
#!/bin/bash
leftDisplay=DisplayPort-1
rightDisplay=HDMI-A-0
xrandr --output ${leftDisplay} --off --output ${rightDisplay} --auto
nitrogen --restore
#+END_SRC
