#!/bin/bash
leftDisplay=DisplayPort-1
rightDisplay=HDMI-A-0
xrandr --output ${leftDisplay} --auto --output ${rightDisplay} --auto --right-of ${leftDisplay}
nitrogen --restore
