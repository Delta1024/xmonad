#!/bin/bash
leftDisplay=DisplayPort-1
rightDisplay=HDMI-A-0
xrandr --output ${leftDisplay} --off --output ${rightDisplay} --auto
nitrogen --restore
