#!/bin/bash
leftDisplay=DP-1
rightDisplay=HDMI-1
xrandr --output ${leftDisplay} --off --output ${rightDisplay} --auto
nitrogen --restore
