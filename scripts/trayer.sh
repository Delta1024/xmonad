#!/bin/bash
v=$(pgrep trayer-srg)

if [ -z "$v" ]
then 
 trayer --edge top --align right --SetDockType true --SetPartialStrut true --expand true --widthtype request --height 19 --tint 0x000000 --monitor 0 --transparent true --alpha 95 &
else
 killall trayer
fi
